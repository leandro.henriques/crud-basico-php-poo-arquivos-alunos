<main>

  <section>
    <a href="<?php echo SITE_BASE . $URL[0] ?>">
      <button class="btn btn-success">Voltar</button>
    </a>
  </section>

  <h2 class="mt-3"><?php echo TITLE; ?></h2>

  <form name="formulario" method="POST">

  <div class="form-group">
      <label for="nome">Nome</label>
      <input type="text" class="form-control" name="nome" value="<?php echo $obUsuario->nome ?? ''; ?>">
    </div>

    <div class="form-group">
      <label for="email">E-mail</label>
      <input type="email" class="form-control" name="email" value="<?php echo $obUsuario->email ?? ''; ?>">
    </div>

    <div class="form-group">
      <label for="senha">Senha</label>
      <input type="password" class="form-control" name="senha">
    </div>

    <div class="form-group">
      <label for="telefone">Telefone</label>
      <input type="telefone" class="form-control" name="telefone" value="<?php echo $obUsuario->telefone ?? ''; ?>">
    </div>

    <div class="form-group">
      <label for="celular">Celular</label>
      <input type="celular" class="form-control" name="celular" value="<?php echo $obUsuario->celular ?? ''; ?>">
    </div>
    
    <div class="form-group mb-4">
      <label>Status</label>
      <select class="form-control" name="status" id="status">
        <option value="" selected>Selecione um status</option>
        <?php foreach ($status as $row) {
          echo '<option value="' . $row['value'] . '" ' . ($row['value'] === $obUsuario->status ? 'selected' : '') . '>' . $row['text'] . '</option>';
        }
        ?>
      </select>
    </div>

    <div class="form-group mb-4">
      <button type="submit" class="btn btn-success">Enviar</button>
    </div>

  </form>

</main>