<?php

$mensagem = '';
if (isset($_GET['status'])) {
    switch ($_GET['status']) {
        case 'success':
            $mensagem = '<div class="alert alert-success">Ação executada com sucesso!</div>';
            break;

        case 'error':
            $mensagem = '<div class="alert alert-danger">Ação não executada!</div>';
            break;
    }
}

$resultados = '';
foreach ($usuarios as $usuario) {
    $resultados .= '<tr>
                      <td>' . $usuario->id . '</td>
                      <td>' . $usuario->nome . '</td>
                      <td>' . $usuario->email . '</td>
                      <td>' . $usuario->status . '</td>
                      <td>' . date('d/m/Y à\s H:i:s', strtotime($usuario->created_at)) . '</td>
                      <td>
                        <a href="editar.php?slug=' . $usuario->slug . '">
                          <button type="button" class="btn btn-primary">Editar</button>
                        </a>
                        <a href="excluir.php?slug=' . $usuario->slug . '">
                          <button type="button" class="btn btn-danger">Excluir</button>
                        </a>
                      </td>
                    </tr>';
}

$resultados = strlen($resultados) ? $resultados : '<tr>
                                                       <td colspan="6" class="text-center">
                                                              Nenhum usuario encontrado
                                                       </td>
                                                    </tr>';

$paginate = '<li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>';
for($i = 0; $i <  count($usuarios); $i++){
    $paginate .= '<li class="page-item"><a class="page-link" href="'. SITE_BASE . $URL[0] . '/' . $URL[1] . '/' . ($i + 1) .'">'. ($i + 1) .'</a></li>';
}
$paginate .= '<li class="page-item"><a class="page-link" href="#">Next</a></li>';
?>
<main>

    <?php echo $mensagem ?>

    <section>
        <a href="<?php echo SITE_BASE . $URL[0] . '/cadastrar'?>">
            <button class="btn btn-success">Novo usuário</button>
        </a>
    </section>

    <section>

        <table class="table bg-light mt-3">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Status</th>
                    <th>Data</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php echo $resultados ?>
            </tbody>
        </table>

    </section>

    <section class="">
    <nav aria-label="Page navigation example">
  <ul class="pagination justify-content-end">
    <?php echo $paginate; ?>
  </ul>
</nav>
    </section>
</main>