<?php

require dirname(__FILE__, 2).'/autoload.php';

define('TITLE','Cadastrar usuário');

use App\Class\Usuario;
use App\Enums\StatusEnum;

$obUsuario = new Usuario;

$status = StatusEnum::toOptions();

//VALIDAÇÃO DO POST
if(isset($_POST['nome'],$_POST['email'],$_POST['status'])){

  $obUsuario->nome      = $_POST['nome'];
  $obUsuario->email     = $_POST['email'];
  $obUsuario->senha     = $_POST['senha'];
  $obUsuario->telefone  = $_POST['telefone'];
  $obUsuario->celular   = $_POST['celular'];
  $obUsuario->status    = $_POST['status'];
  $obUsuario->cadastrar();

  header('location: '.SITE_BASE.'usuarios/?status=success');
  exit;
}

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/usuarios/includes/formulario.php';
include dirname(__FILE__, 2).'/includes/footer.php';