<?php

require dirname(__FILE__, 2) . '/autoload.php';

define('TITLE', 'Editar usuário');

use App\Class\Usuario;
use App\Enums\StatusEnum;

$slug = htmlspecialchars($_GET['slug'], ENT_QUOTES, 'ISO-8859-1', true);
$status = StatusEnum::toOptions();

//VALIDAÇÃO DO SLUG
if (!isset($_GET['slug']) or empty($slug)) {
    header('location: '.SITE_BASE.'usuarios/?status=error');
    exit;
}

//CONSULTA O USUÁRIO
$obUsuario = Usuario::getUsuario($slug);

//VALIDAÇÃO DO USUÁRIO
if (!$obUsuario instanceof Usuario) {
    header('location: '.SITE_BASE.'usuarios/?status=error');
    exit;
}

//VALIDAÇÃO DO POST
if (isset($_POST['nome'], $_POST['email'], $_POST['status'])) {

    $obUsuario->nome      = $_POST['nome'];
    $obUsuario->email     = $_POST['email'];
    $obUsuario->senha     = $_POST['senha'] ?? $obUsuario->senha;
    $obUsuario->telefone  = $_POST['telefone'];
    $obUsuario->celular   = $_POST['celular'];
    $obUsuario->status    = $_POST['status'];
    $obUsuario->atualizar();

    header('location: '.SITE_BASE.'usuarios/?status=success');
    exit;
}

include dirname(__FILE__, 2) . '/includes/header.php';
include dirname(__FILE__, 2) . '/usuarios/includes/formulario.php';
include dirname(__FILE__, 2) . '/includes/footer.php';
