<?php

require dirname(__FILE__, 2).'/autoload.php';

use App\Class\Usuario;

$slug = htmlspecialchars($_GET['slug'], ENT_QUOTES,'ISO-8859-1', true);

//VALIDAÇÃO DO SLUG
if (!isset($_GET['slug']) or empty($slug)) {
  header('location: '.SITE_BASE.'usuarios/?status=error');
  exit;
}

//CONSULTA O USUÁRIO
$obUsuario = Usuario::getUsuario($slug);

//VALIDAÇÃO DO USUÁRIO
if(!$obUsuario instanceof Usuario){
  header('location: '.SITE_BASE.'usuarios/?status=error');
  exit;
}

//VALIDAÇÃO DO POST
if(isset($_POST['excluir'])){

  $obUsuario->excluir();

  header('location: '.SITE_BASE.'usuarios/?status=success');
  exit;
}

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/usuarios/includes/confirmar-exclusao.php';
include dirname(__FILE__, 2).'/includes/footer.php';