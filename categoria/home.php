<?php

require dirname(__FILE__, 2).'/autoload.php';

use App\Class\Categoria;

$pgUrl = $URL[2] ?? 0;
$limit = 10;
$page =  ($pgUrl > 0 ) ? $pgUrl - 1 : 0;
$usuarios = Categoria::getUsuarios(null, 'nome asc', "$page, $limit");

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/usuarios/includes/listagem.php';
include dirname(__FILE__, 2).'/includes/footer.php';