<?php

require dirname(__FILE__, 2) . '/autoload.php';

define('TITLE', 'Editar categoria');

use App\Class\Categoria;
use App\Enums\StatusEnum;

$slug = htmlspecialchars($_GET['slug'], ENT_QUOTES, 'ISO-8859-1', true);
$status = StatusEnum::toOptions();

//VALIDAÇÃO DO SLUG
if (!isset($_GET['slug']) or empty($slug)) {
    header('location: '.SITE_BASE.'categoria/?status=error');
    exit;
}

//CONSULTA A CATEGORIA
$obCategoria = Categoria::getCategoria($slug);

//VALIDAÇÃO DO USUÁRIO
if (!$obCategoria instanceof Categoria) {
    header('location: '.SITE_BASE.'categoria/?status=error');
    exit;
}

//VALIDAÇÃO DO POST
if (isset($_POST['nome'])) {

    $obCategoria->nome      = $_POST['nome'];
    $obCategoria->atualizar();

    header('location: '.SITE_BASE.'categoria/?status=success');
    exit;
}

include dirname(__FILE__, 2) . '/includes/header.php';
include dirname(__FILE__, 2) . '/categoria/includes/formulario.php';
include dirname(__FILE__, 2) . '/includes/footer.php';
