<?php

require dirname(__FILE__, 2).'/autoload.php';

define('TITLE','Cadastrar categoria');

use App\Class\Categoria;
use App\Enums\StatusEnum;

$obCategoria = new Categoria;

$status = StatusEnum::toOptions();

//VALIDAÇÃO DO POST
if(isset($_POST['nome'])){

  $obCategoria->nome      = $_POST['nome'];
  $obCategoria->cadastrar();

  header('location: '.SITE_BASE.'categoria/?status=success');
  exit;
}

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/categoria/includes/formulario.php';
include dirname(__FILE__, 2).'/includes/footer.php';