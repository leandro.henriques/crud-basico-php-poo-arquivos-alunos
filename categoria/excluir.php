<?php

require dirname(__FILE__, 2).'/autoload.php';

use App\Class\Categoria;

$slug = htmlspecialchars($_GET['slug'], ENT_QUOTES,'ISO-8859-1', true);

//VALIDAÇÃO DO SLUG
if (!isset($_GET['slug']) or empty($slug)) {
  header('location: '.SITE_BASE.'categoria/?status=error');
  exit;
}

//CONSULTA A CATEGORIA
$obCategoria = Categoria::getCategoria($slug);

//VALIDAÇÃO DA CATEGORIA
if(!$obCategoria instanceof Categoria){
  header('location: '.SITE_BASE.'categoria/?status=error');
  exit;
}

//VALIDAÇÃO DO POST
if(isset($_POST['excluir'])){
  $obCategoria->excluir();

  header('location: '.SITE_BASE.'categoria/?status=success');
  exit;
}

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/categoria/includes/confirmar-exclusao.php';
include dirname(__FILE__, 2).'/includes/footer.php';