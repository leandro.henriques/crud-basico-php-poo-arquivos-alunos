<!doctype html>
<html lang="pt-br">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Aula Desenvolvimento Web!</title>
</head>

<body class="bg-light">
    <section class="vh-100">
        <div class="container-fluid h-custom">
            <div class="row d-flex justify-content-center align-items-center h-100 my-5">
                <div class="col-md-9 col-lg-6 col-xl-5">
                    <img src="/assets/images/draw2.webp" class="img-fluid" alt="Sample image">
                </div>
                <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
                    <form>
                        <div class="form-group mb-3">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control form-control-sm" name="nome"
                                value="<?php echo $obUsuario->nome ?? ''; ?>">
                        </div>

                        <div class="form-group mb-3">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control form-control-sm" name="email"
                                value="<?php echo $obUsuario->email ?? ''; ?>">
                        </div>

                        <div class="form-group mb-3">
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control form-control-sm" name="senha">
                        </div>

                        <div class="form-group mb-3">
                            <label for="telefone">Telefone</label>
                            <input type="telefone" class="form-control form-control-sm" name="telefone"
                                value="<?php echo $obUsuario->telefone ?? ''; ?>">
                        </div>

                        <div class="form-group mb-3">
                            <label for="celular">Celular</label>
                            <input type="celular" class="form-control form-control-sm" name="celular"
                                value="<?php echo $obUsuario->celular ?? ''; ?>">
                        </div>

                        <div class="text-center text-lg-start mt-4 pt-2">
                            <button type="button" class="btn btn-primary btn-sm"
                                style="padding-left: 2.5rem; padding-right: 2.5rem;">Cadastro</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
</body>

</html>