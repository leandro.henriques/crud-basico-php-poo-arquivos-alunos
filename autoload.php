<?php

// Define o diretório base do projeto
@define('SITE_BASE', "http://localhost/crud-basico-php-poo/");

// session_start();
// if(!isset($_SESSION['auth'])){
//     $pos = strripos(filter_input(INPUT_SERVER, 'REQUEST_URI'), 'login');
//     if ($pos === false) {
//         header('location: '.SITE_BASE.'login');
//         exit;
//     }
// }

$baseDir = __DIR__ . '/';
$REQUEST_URI = filter_input(INPUT_SERVER, 'REQUEST_URI');
$INITE = strpos($REQUEST_URI, '?');
if ($INITE):
    $REQUEST_URI = substr($REQUEST_URI, 0, $INITE);
endif;
$URL = explode('/', str_replace('crud-basico-php-poo/', '', substr($REQUEST_URI, 1)));

// Função de autoloading
spl_autoload_register(function ($class) use ($baseDir) {
    // Converte o namespace da classe em um caminho de diretório
    $class = str_replace('\\', '/', $class);

    // Monta o caminho completo do arquivo da classe
    $file = $baseDir . $class . '.php';

    // Verifica se o arquivo existe e o inclui
    if (file_exists($file)) {
        require $file;
    }
});