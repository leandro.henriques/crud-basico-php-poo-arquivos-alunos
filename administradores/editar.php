<?php

require dirname(__FILE__, 2) . '/autoload.php';

define('TITLE', 'Editar administrador');

use App\Class\Administrador;
use App\Enums\StatusEnum;

$slug = htmlspecialchars($_GET['slug'], ENT_QUOTES, 'ISO-8859-1', true);
$status = StatusEnum::toOptions();

//VALIDAÇÃO DO SLUG
if (empty($slug)) {
    header('location: '.SITE_BASE.'administradores/?status=error');
    exit;
}

//CONSULTA O ADMINISTRADOR
$obAdministrador = Administrador::getAdministrador($slug);

//VALIDAÇÃO DO ADMINISTRADOR
if (!$obAdministrador instanceof Administrador) {
    header('location: '.SITE_BASE.'administradores/?status=error');
    exit;
}

//VALIDAÇÃO DO POST
if (isset($_POST['nome'], $_POST['email'], $_POST['status'])) {

    $obAdministrador->nome      = $_POST['nome'];
    $obAdministrador->email     = $_POST['email'];
    $obAdministrador->senha     = $_POST['senha'] ?? $obAdministrador->senha;
    $obAdministrador->telefone  = $_POST['telefone'];
    $obAdministrador->celular   = $_POST['celular'];
    $obAdministrador->status    = $_POST['status'];
    $obAdministrador->atualizar();

    header('location: '.SITE_BASE.'administradores/?status=success');
    exit;
}

include dirname(__FILE__, 2) . '/includes/header.php';
include dirname(__FILE__, 2) . '/administradores/includes/formulario.php';
include dirname(__FILE__, 2) . '/includes/footer.php';
