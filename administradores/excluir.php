<?php

require dirname(__FILE__, 2).'/autoload.php';

use App\Class\Administrador;

$slug = htmlspecialchars($_GET['slug'], ENT_QUOTES,'ISO-8859-1', true);

//VALIDAÇÃO DO SLUG
if (!isset($_GET['slug']) or empty($slug)) {
  header('location: '.SITE_BASE.'administradores/?status=error');
  exit;
}

//CONSULTA O ADMINISTRADOR
$obAdministrador = Administrador::getAdministrador($slug);

//VALIDAÇÃO DO ADMINISTRADOR
if(!$obAdministrador instanceof Administrador){
  header('location: '.SITE_BASE.'administradores/?status=error');
  exit;
}

//VALIDAÇÃO DO POST
if(isset($_POST['excluir'])){

  $obAdministrador->excluir();

  header('location: '.SITE_BASE.'administradores/?status=success');
  exit;
}

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/administradores/includes/confirmar-exclusao.php';
include dirname(__FILE__, 2).'/includes/footer.php';