<?php

require dirname(__FILE__, 2).'/autoload.php';

use App\Class\Administrador;

$pgUrl = $URL[2] ?? 0;
$limit = 10;
$page =  ($pgUrl > 0 ) ? $pgUrl - 1 : 0;
$administradores = Administrador::getAdministradores(null, 'nome asc', "$page, $limit");

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/administradores/includes/listagem.php';
include dirname(__FILE__, 2).'/includes/footer.php';