<?php

require dirname(__FILE__, 2).'/autoload.php';

define('TITLE','Cadastrar administrador');

use App\Class\Administrador;
use App\Enums\StatusEnum;

$obAdminitrador = new Administrador;

$status = StatusEnum::toOptions();

//VALIDAÇÃO DO POST
if(isset($_POST['nome'],$_POST['email'],$_POST['status'])){

  $obAdminitrador->nome      = $_POST['nome'];
  $obAdminitrador->email     = $_POST['email'];
  $obAdminitrador->senha     = $_POST['senha'];
  $obAdminitrador->status    = $_POST['status'];
  $obAdminitrador->cadastrar();

  header('location: '.SITE_BASE.'administradores/?status=success');
  exit;
}

include dirname(__FILE__, 2).'/includes/header.php';
include dirname(__FILE__, 2).'/administradores/includes/formulario.php';
include dirname(__FILE__, 2).'/includes/footer.php';