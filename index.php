<?php

require __DIR__.'/autoload.php';

$URL[0] = ($URL[0] != '' ? $URL[0] : 'home');

if (file_exists(__DIR__ . '\\'. $URL[0] . '.php')):
    require(__DIR__ . '\\'. $URL[0] . '.php');
elseif (is_dir(__DIR__ . '\\'. $URL[0])):
    $URL[1] = ($URL[1] != '' ? $URL[1] : 'home');
    if (isset($URL[1]) && file_exists(__DIR__ . '\\'. $URL[0] . '\\' . $URL[1] . '.php')):
        require(__DIR__ . '\\'. $URL[0] . '\\' . $URL[1] . '.php');
    else:
        require(__DIR__ . '\\Errors\\404.php');
    endif;
else:
    require(__DIR__ . '\\Errors\\404.php');
endif;