<?php

namespace App\Support;

class Str
{
    private static array $opcoes = ['cost' => 12,];
    /**
     * Converta a string fornecida para letras minúsculas.
     *
     * @param  string  $value
     * @return string
     */
    public static function lower($value)
    {
        return mb_strtolower($value, 'UTF-8');
    }

    /**
     * Cria uma senha criptografada usando o algoritmo PASSWORD_ARGON2ID
     *
     * @param  string  $senha
     * @return string
     */
    public static function Password($senha)
    {
        return password_hash($senha, PASSWORD_ARGON2ID, static::$opcoes);
    }

    /**
     * Gere um "slug" compatível com URL a partir de uma determinada string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @param  string|null  $language
     * @param  array<string, string>  $dictionary
     * @return string
     */
    public static function slug($title, $separator = '-', $language = 'en', $dictionary = ['@' => 'at'])
    {
        // Converter todos os traços/sublinhados em separador
        $flip = $separator === '-' ? '_' : '-';

        $title = preg_replace('![' . preg_quote($flip) . ']+!u', $separator, $title);

        // Substituir palavras do dicionário
        foreach ($dictionary as $key => $value) {
            $dictionary[$key] = $separator . $value . $separator;
        }

        $title = str_replace(array_keys($dictionary), array_values($dictionary), $title);

        // Remova todos os caracteres que não sejam separadores, letras, números ou espaços em branco
        $title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', static::lower($title));

        // Substitua todos os caracteres separadores e espaços em branco por um único separador
        $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);

        return trim($title, $separator);
    }

    /**
     * Valida a senha
     *
     * @param  string  $senha
     * @param  string  $senhaCriptografada
     * @return boolean
     */
    public static function ValidPassword(string $senha = null, string $senhaCriptografada = null)
    {
        if (password_verify($senha, $senhaCriptografada)) {
            return true;
        } else {
            return false;
        }
    }
}
