<?php

namespace App\Traits;

use App\Db\Database;
use App\Support\Str;

/**
 * Automatically create slug based on selected field
 *
 * @return string
 */
trait SlugTrait
{
    protected static function updateSlugTrait($slugFromField)
    {
        if (!is_null($slugFromField)) {
            return $slugFromField . '-' . uniqid();
        }
    }

    public static function checkSlugTrait(string $slugFromField = NULL, string $table = NULL, int $id = 0)
    {
        if (is_null($slugFromField) || is_null($table)) {
            return 'Error não foi possível atender sua solicitação';
        }

        $slugFromField = Str::slug($slugFromField);
        $data = (new Database($table))->select('slug = \'' . $slugFromField . '\'')->fetchObject(self::class);

        if(empty($data) || ($id > 0 && $id !== $data->id)){
            return static::updateSlugTrait($slugFromField);
        }else{
            return $slugFromField;
        }
    }
}
