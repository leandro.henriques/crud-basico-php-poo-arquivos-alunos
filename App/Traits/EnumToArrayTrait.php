<?php

namespace App\Traits;

trait EnumToArrayTrait
{
    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function array(): array
    {
        return array_combine(self::values(), self::names());
    }

    public static function toOptions($selected = []): object
    {
        $element = [];
        $data = self::array();

        if (!empty($selected)) {
            if (is_array($selected)) {
                foreach ($selected as $row) {
                    foreach ($data as $key => $value) {
                        if ($row === $key) {
                            array_push($element, ['value' => $key, 'text' => $value]);
                        }
                    }
                }
            } else {
                foreach ($data as $key => $value) {
                    if ($selected === $key) {
                        array_push($element, ['value' => $key, 'text' => $value]);
                    }
                }
            }
        } else {
            foreach ($data as $key => $value) {
                array_push($element, ['value' => $key, 'text' => $value]);
            }
        }

        return (object) $element;
    }
}
