<?php

namespace App\Enums;

use App\Traits\EnumToArrayTrait;

enum StatusEnum: string
{
    use EnumToArrayTrait;
    
    case Active     = 'active';
    case Deleted    = 'deleted';
    case Inactive   = 'inactive';
    case Pending    = 'pending';
    case Rejected   = 'rejected';
    case Waiting    = 'waiting';

    public static function getAllValues(): array
    {
        return array_column(StatusEnum::cases(), 'keys');
    }
}
