DROP DATABASE IF EXISTS aula_php;
CREATE DATABASE IF NOT EXISTS aula_php;
USE aula_php;

DROP TABLE IF EXISTS usuarios, endereco_usuario, categoria, produtos, carrinho, carrinho_item, endereco_entrega, administrador;

CREATE TABLE usuarios (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  nome varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  email varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  senha varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  telefone varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  celular varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  status enum('active','pending','inactive','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  slug varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE endereco_usuario(
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  usuario_id bigint unsigned NOT NULL,
  nome varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  endereco varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  complemento varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  numero int(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  bairro varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  cidade varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  estado varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  cep varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE categorias(
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  nome bigint unsigned NOT NULL,
  slug varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE produtos(
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  categoria_id bigint unsigned,
  nome varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  breve_descricao varchar(255) COLLATE utf8mb4_unicode_ci,
  descricao text COLLATE utf8mb4_unicode_ci,
  foto varchar(255) COLLATE utf8mb4_unicode_ci,
  valor decimal(12,2),
  desconto decimal(10,2),
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE carrinho(
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  usuario_id bigint unsigned,
  ip varchar(20) NOT NULL,
  status enum('comprando','aguardando_pagamento','pago','cancelado') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'comprando',
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE carrinho_item (
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  carrinho_id bigint unsigned,
  produto_id bigint unsigned,
  quantidade  int(11),
  valor_produto decimal(12,2),
  desconto decimal(12,2),
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE endereco_entrega(
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  carrinho_id bigint unsigned NOT NULL,
  nome varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  endereco varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  complemento varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  numero int(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  bairro varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  cidade varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  estado varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  cep varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE administrador(
  id bigint unsigned NOT NULL AUTO_INCREMENT,
  nome varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  email varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  senha varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, 
  status enum('active','pending','inactive','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  slug varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
);