<?php

namespace App\Db;

use App\Db\Database;

class Install_database extends Database
{
    /**
     * Método responsável por criar as tabelas no banco de dados
     * @return boolean
     */
    public function instalar()
    {
        $path = __DIR__ . '/SQL/';

        $arquivos = glob("$path{*.sql}", GLOB_BRACE);
        $db = new Database();

        foreach ($arquivos as $row) {
            $sql = file_get_contents($row);
            $db->createTable($sql);
        }
        $db->fecharConexao();
        return true;
    }
}