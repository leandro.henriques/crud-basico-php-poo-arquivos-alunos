<?php

namespace App\Interface;

interface IAdministrador
{
    public function cadastrar();

    public function atualizar();

    public function excluir();

    public static function getAdministradores();

    public static function getAdministrador();
}
