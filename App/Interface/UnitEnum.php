<?php

namespace App\Interface;

interface UnitEnum
{
    public static function getAllValues(): array;
}
