<?php

namespace App\Interface;

interface ICategoria
{
    public function cadastrar();

    public function atualizar();

    public function excluir();

    public static function getCategorias();

    public static function getCategoria();
}
