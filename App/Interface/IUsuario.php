<?php

namespace App\Interface;

interface IUsuario
{
    public function cadastrar();

    public function atualizar();

    public function excluir();

    public static function getUsuarios();

    public static function getUsuario();
}
