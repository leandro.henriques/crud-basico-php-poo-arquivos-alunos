<?php

namespace App\Class;

use PDO;
use App\Db\Database;
use App\Support\Str;
use App\Traits\SlugTrait;
use App\Interface\ICategoria;

class Categoria implements ICategoria
{
  use SlugTrait;

  /**
   * Identificador único da categoria
   * @var integer
   */
  public $id;

  /**
   * Nome da categoria
   * @var string
   */
  public $nome;

  /**
   * Define nome amigavel para a categoria
   * @var string
   */
  public $slug;

  /**
   * Data de cadastro da categoria
   * @var string
   */
  public $data;
  
  /**
   * Método responsável por cadastrar uma nova categoria no banco
   * @return boolean
   */
  public function cadastrar()
  {
    //DEFINIR A DATA
    $this->data = date('Y-m-d H:i:s');

    //INSERIR A CATEGORIA NO BANCO
    $obDatabase = new Database('categorias');

    $this->id = $obDatabase->insert([
      'nome'       => $this->nome,
      'slug'       => static::checkSlugTrait($this->nome, 'categorias'),
      'created_at' => $this->data,
      'updated_at' => $this->data,
    ]);

    //RETORNAR SUCESSO
    return true;
  }

  /**slug
   * Método responsável por atualizar a categoria no banco
   * @param  integer $id
   * @return boolean
   */
  public function atualizar()
  {
    $this->data = date('Y-m-d H:i:s');

    return (new Database('categorias'))->update('id = ' . $this->id, [
      'nome'       => $this->nome,
      'slug'       => static::checkSlugTrait($this->nome, 'usuarios', $this->id),
      'updated_at' => $this->data,
    ]);
  }

  /**
   * Método responsável por excluir a categoria do banco
   * @param  string $slug
   * @return boolean
   */
  public function excluir()
  {
    return (new Database('categorias'))->delete('slug = \''.$this->slug.'\'');
  }

  /**
   * Método responsável por obter as categorias do banco de dados
   * @param  string $where
   * @param  string $order
   * @param  string $limit
   * @return array
   */
  public static function getCategorias($where = null, $order = null, $limit = null)
  {
    return (new Database('categorias'))->select($where, $order, $limit)
      ->fetchAll(PDO::FETCH_CLASS, self::class);
  }

  /**
   * Método responsável por buscar uma categoria com base em seu slug
   * @param  string $slug
   * @return Usuario
   */
  public static function getCategoria(string $slug = '')
  {
    return (new Database('categorias'))->select('slug = \''.$slug.'\'')
      ->fetchObject(self::class);
  }
}
