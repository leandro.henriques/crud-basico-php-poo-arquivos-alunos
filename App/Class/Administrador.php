<?php

namespace App\Class;

use PDO;
use App\Db\Database;
use App\Support\Str;
use App\Traits\SlugTrait;
use App\Interface\IAdministrador;

class Administrador implements IAdministrador
{
  use SlugTrait;

  /**
   * Identificador único do administrador
   * @var integer
   */
  public $id;

  /**
   * Nome do administrador
   * @var string
   */
  public $nome;

  /**
   * E-mail do adminitrador
   * @var string
   */
  public $email;

  /**
   * Senha do administrador
   * @var string
   */
  public $senha;

  /**
   * Define se o status do administrador
   * @var string
   */
  public $status;

  /**
   * Define nome amigavel para o administrador
   * @var string
   */
  public $slug;

  /**
   * Data de cadastro do administrador
   * @var string
   */
  public $data;
  
  /**
   * Método responsável por cadastrar um novo administrador no banco
   * @return boolean
   */
  public function cadastrar()
  {
    //DEFINIR A DATA
    $this->data = date('Y-m-d H:i:s');

    //INSERIR O ADMINISTRADOR NO BANCO
    $obDatabase = new Database('administrador');

    $this->id = $obDatabase->insert([
      'nome'       => $this->nome,
      'email'      => $this->email,
      'senha'      => Str::Password($this->senha),
      'status'     => $this->status,
      'slug'       => static::checkSlugTrait($this->nome, 'usuarios'),
      'created_at' => $this->data,
      'updated_at' => $this->data,
    ]);

    //RETORNAR SUCESSO
    return true;
  }

  /**slug
   * Método responsável por atualizar o Administrador no banco
   * @param  integer $id
   * @return boolean
   */
  public function atualizar()
  {
    $this->data = date('Y-m-d H:i:s');

    return (new Database('administrador'))->update('id = ' . $this->id, [
      'nome'       => $this->nome,
      'email'      => $this->email,
      'senha'      => Str::Password($this->senha),
      'status'     => $this->status,
      'slug'       => static::checkSlugTrait($this->nome, 'usuarios', $this->id),
      'updated_at' => $this->data,
    ]);
  }

  /**
   * Método responsável por excluir O administrador do banco
   * @param  string $slug
   * @return boolean
   */
  public function excluir()
  {
    return (new Database('administrador'))->delete('slug = \''.$this->slug.'\'');
  }

  /**
   * Método responsável por obter os administradores do banco de dados
   * @param  string $where
   * @param  string $order
   * @param  string $limit
   * @return array
   */
  public static function getAdministradores($where = null, $order = null, $limit = null)
  {
    return (new Database('administrador'))->select($where, $order, $limit)
      ->fetchAll(PDO::FETCH_CLASS, self::class);
  }

  /**
   * Método responsável por buscar um administrador com base em seu slug
   * @param  string $slug
   * @return Administrador
   */
  public static function getAdministrador(string $slug = '')
  {
    return (new Database('administrador'))->select('slug = \''.$slug.'\'')
      ->fetchObject(self::class);
  }
}
