<?php

namespace App\Class;

use PDO;
use App\Db\Database;
use App\Support\Str;
use App\Traits\SlugTrait;
use App\Interface\IUsuario;

class Usuario implements IUsuario
{
  use SlugTrait;

  /**
   * Identificador único do usuário
   * @var integer
   */
  public $id;

  /**
   * Nome do usuário
   * @var string
   */
  public $nome;

  /**
   * E-mail do usuário
   * @var string
   */
  public $email;

  /**
   * Senha do usuário
   * @var string
   */
  public $senha;

  /**
   * Telefone do usuário
   * @var string
   */
  public $telefone;

  /**
   * Celular do usuário
   * @var string
   */
  public $celular;

  /**
   * Define se o usuário esta status ou não
   * @var string
   */
  public $status;

  /**
   * Define nome amigavel para o usuário
   * @var string
   */
  public $slug;

  /**
   * Data de cadastro do usuário
   * @var string
   */
  public $data;
  
  /**
   * Método responsável por cadastrar um novo usuario no banco
   * @return boolean
   */
  public function cadastrar()
  {
    //DEFINIR A DATA
    $this->data = date('Y-m-d H:i:s');
    // static::$slugFromField = $this->slug;

    //INSERIR O USUÁRIO NO BANCO
    $obDatabase = new Database('usuarios');

    $this->id = $obDatabase->insert([
      'nome'       => $this->nome,
      'email'      => $this->email,
      'senha'      => Str::Password($this->senha),
      'telefone'   => $this->telefone,
      'celular'    => $this->celular,
      'status'     => $this->status,
      'slug'       => static::checkSlugTrait($this->nome, 'usuarios'),
      'created_at' => $this->data,
      'updated_at' => $this->data,
    ]);

    //RETORNAR SUCESSO
    return true;
  }

  /**slug
   * Método responsável por atualizar o usuário no banco
   * @param  integer $id
   * @return boolean
   */
  public function atualizar()
  {
    $this->data = date('Y-m-d H:i:s');

    return (new Database('usuarios'))->update('id = ' . $this->id, [
      'nome'       => $this->nome,
      'email'      => $this->email,
      'senha'      => Str::Password($this->senha),
      'telefone'   => $this->telefone,
      'celular'    => $this->celular,
      'status'     => $this->status,
      'slug'       => static::checkSlugTrait($this->nome, 'usuarios', $this->id),
      'updated_at' => $this->data,
    ]);
  }

  /**
   * Método responsável por excluir O usuário do banco
   * @param  string $slug
   * @return boolean
   */
  public function excluir()
  {
    return (new Database('usuarios'))->delete('slug = \''.$this->slug.'\'');
  }

  /**
   * Método responsável por obter as usuarios do banco de dados
   * @param  string $where
   * @param  string $order
   * @param  string $limit
   * @return array
   */
  public static function getUsuarios($where = null, $order = null, $limit = null)
  {
    return (new Database('usuarios'))->select($where, $order, $limit)
      ->fetchAll(PDO::FETCH_CLASS, self::class);
  }

  /**
   * Método responsável por buscar um usuario com base em seu slug
   * @param  string $slug
   * @return Usuario
   */
  public static function getUsuario(string $slug = '')
  {
    return (new Database('usuarios'))->select('slug = \''.$slug.'\'')
      ->fetchObject(self::class);
  }
}
